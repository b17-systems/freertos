#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

#include "main.h"

/* Ensure stdint is only used by the compiler, and not the assembler. */
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
    #include <stdint.h>
    extern uint32_t SystemCoreClock;
#endif

#define configUSE_PREEMPTION                    1
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 1
#define configUSE_TICKLESS_IDLE                 0
#define configCPU_CLOCK_HZ                      ( SystemCoreClock )
/* STM32G4 Series Reference Manual (RM0440), Reset and Clock Control (RCC), page 277:
 * "The RCC feeds the Cortex® System Timer (SysTick) external clock with the AHB clock (HCLK) divided by 8."
 * */
#define configSYSTICK_CLOCK_HZ                  ( ( SystemCoreClock ) / 8 )
#define configTICK_RATE_HZ                      ( ( TickType_t ) 1000 )
#define configMAX_PRIORITIES                    5
#define configMINIMAL_STACK_SIZE                128
#define configMAX_TASK_NAME_LEN                 16
#define configUSE_16_BIT_TICKS                  0
#define configIDLE_SHOULD_YIELD                 1
#define configUSE_TASK_NOTIFICATIONS            1
#define configTASK_NOTIFICATION_ARRAY_ENTRIES   3
#define configUSE_MUTEXES                       1
#define configUSE_RECURSIVE_MUTEXES             0
#define configUSE_COUNTING_SEMAPHORES           0
#define configQUEUE_REGISTRY_SIZE               8
#define configUSE_QUEUE_SETS                    0
#define configUSE_TIME_SLICING                  0
#define configUSE_NEWLIB_REENTRANT              0
#define configENABLE_BACKWARD_COMPATIBILITY     0
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS 5
#define configSTACK_DEPTH_TYPE                  uint16_t
#define configMESSAGE_BUFFER_LENGTH_TYPE        size_t

/* Memory allocation related definitions. */
#define configSUPPORT_STATIC_ALLOCATION         0
#define configSUPPORT_DYNAMIC_ALLOCATION        1
#define configTOTAL_HEAP_SIZE                   ( ( size_t ) ( 8 * 1024 ) )
#define configAPPLICATION_ALLOCATED_HEAP        0

/* Hook function related definitions. */
#define configUSE_IDLE_HOOK                     0
#define configUSE_TICK_HOOK                     0
#define configCHECK_FOR_STACK_OVERFLOW          0
#define configUSE_MALLOC_FAILED_HOOK            0
#define configUSE_DAEMON_TASK_STARTUP_HOOK      0

/* Run time and task stats gathering related definitions. */
#define configGENERATE_RUN_TIME_STATS           0
#define configUSE_TRACE_FACILITY                0
#define configUSE_STATS_FORMATTING_FUNCTIONS    0

/* Co-routine related definitions. */
#define configUSE_CO_ROUTINES                   0
#define configMAX_CO_ROUTINE_PRIORITIES         1

/* Software timer related definitions. */
#define configUSE_TIMERS                        1
#define configTIMER_TASK_PRIORITY		        ( configMAX_PRIORITIES - 1 )
#define configTIMER_QUEUE_LENGTH                10
#define configTIMER_TASK_STACK_DEPTH            configMINIMAL_STACK_SIZE

/* Interrupt nesting behaviour configuration. */

#ifdef __NVIC_PRIO_BITS
    #define configPRIO_BITS                     __NVIC_PRIO_BITS
#else
    #define configPRIO_BITS                     4  /* 15 priority levels */
#endif

/* configKERNEL_INTERRUPT_PRIORITY sets the interrupt priority used
 * by the RTOS kernel itself. It should be set to the lowest priority.
 * */
#define configKERNEL_INTERRUPT_PRIORITY         ( (uint8_t) (0xff << ( 8 - configPRIO_BITS )) )

/* configMAX_SYSCALL_INTERRUPT_PRIORITY sets the highest interrupt priority
 * from which interrupt safe FreeRTOS API functions can be called.
 * Any interrupt service routine that uses a FreeRTOS API function must have
 * its priority manually set to a value that is numerically equal to or
 * greater than the configMAX_SYSCALL_INTERRUPT_PRIORITY setting.
 *
 * Cortex-M interrupts default to having a priority value of zero.
 * Zero is the highest possible priority value.
 * Therefore, never leave the priority of an interrupt that uses the
 * interrupt safe RTOS API at its default value.
 * */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY    ( 5 << ( 8 - configPRIO_BITS ) )

/* Special note to ARM Cortex-M users:
 * ARM Cortex-M3, ARM Cortex-M4 and ARM Cortex-M4F ports need FreeRTOS handlers to be installed on the SysTick,
 * PendSV and SVCCall interrupt vectors. The vector table can be populated directly with the FreeRTOS defined
 * xPortSysTickHandler(), xPortPendSVHandler() and vPortSVCHandler() functions respectively, or,
 * if the interrupt vector table is CMSIS compliant, the following three lines can be added to
 * FreeRTOSConfig.h to map the FreeRTOS function names to their CMSIS equivalents:
 * */
#define vPortSVCHandler     SVC_Handler
#define xPortPendSVHandler  PendSV_Handler
#define xPortSysTickHandler SysTick_Handler


/* Define to trap errors during development. */
#define configASSERT( x ) if( ( x ) == 0 ) vAssertCalled( __FILE__, __LINE__ )

/* FreeRTOS MPU specific definitions. */
/* #define configINCLUDE_APPLICATION_DEFINED_PRIVILEGED_FUNCTIONS 0
 * #define configTOTAL_MPU_REGIONS                                8
 * #define configTEX_S_C_B_FLASH                                  0x07UL
 * #define configTEX_S_C_B_SRAM                                   0x07UL
 * #define configENFORCE_SYSTEM_CALLS_FROM_KERNEL_ONLY            1
 * */

/* Optional functions - most linkers will remove unused functions anyway. */
#define INCLUDE_vTaskPrioritySet                1
#define INCLUDE_uxTaskPriorityGet               1
#define INCLUDE_vTaskDelete                     1
#define INCLUDE_vTaskSuspend                    1
#define INCLUDE_xResumeFromISR                  1
#define INCLUDE_vTaskDelayUntil                 1
#define INCLUDE_vTaskDelay                      1
#define INCLUDE_xTaskGetSchedulerState          1
#define INCLUDE_xTaskGetCurrentTaskHandle       1
#define INCLUDE_uxTaskGetStackHighWaterMark     0
#define INCLUDE_xTaskGetIdleTaskHandle          0
#define INCLUDE_eTaskGetState                   0
#define INCLUDE_xEventGroupSetBitFromISR        1
#define INCLUDE_xTimerPendFunctionCall          0
#define INCLUDE_xTaskAbortDelay                 0
#define INCLUDE_xTaskGetHandle                  0
#define INCLUDE_xTaskResumeFromISR              1

/* A header file that defines trace macros can be included here. */

#endif /* FREERTOS_CONFIG_H */